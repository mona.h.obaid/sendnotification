package com.effects.requestnotification;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.effects.requestnotification.app.AppController;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    String serverURL="https://fbnoti.000webhostapp.com/fcm/fcm_insert.php";
            //"https://fbnoti.000webhostapp.com/fcm/fcm_insert.php";
    String serverURL_SendNotification="https://fbnoti.000webhostapp.com/fcm/send_notification.php";
    EditText title ;
    EditText message ;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button sendBtn=findViewById(R.id.send_btn);
        Button sendNotification=findViewById(R.id.sendNotification_btn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences(getString(R.string.fcm_pref), Context.MODE_PRIVATE);
                final String token = sharedPreferences.getString(getString(R.string.fcm_token),"");
                Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
               sendRequest();


            }
        });

        sendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                sendNotification();


            }
        });
    }

    //------------------------------- send request --------------------

    public void sendRequest() {

        SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences(getString(R.string.fcm_pref), Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString(getString(R.string.fcm_token),"");

        StringRequest stringRequest= new StringRequest(Request.Method.POST, serverURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MainActivity.this, "Send Success", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("fcm_token",token);

                return map;


            }
        };


        AppController.getInstance(MainActivity.this).addToRequestQueue(stringRequest);

    }
  //------------------------------------- send Notification ---------------------
    public void sendNotification() {

        final  EditText title = findViewById(R.id.textTitle);
        final  EditText message =findViewById(R.id.textMessage);
        SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences(getString(R.string.fcm_pref), Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString(getString(R.string.fcm_token),"");

        StringRequest stringRequest= new StringRequest(Request.Method.POST, serverURL_SendNotification, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MainActivity.this, "Send Success", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("title",title.getText().toString());
                map.put("message",message.getText().toString());
                map.put("fcm_token",token);

                return map;


            }
        };


        AppController.getInstance(MainActivity.this).addToRequestQueue(stringRequest);

    }
}

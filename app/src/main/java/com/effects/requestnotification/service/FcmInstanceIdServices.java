package com.effects.requestnotification.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import com.effects.requestnotification.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FcmInstanceIdServices extends FirebaseInstanceIdService {
    public FcmInstanceIdServices() {
    }

    @Override
    public void onTokenRefresh() {
        String recent_token= FirebaseInstanceId.getInstance().getToken();  // get token from firebase
        SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences(getString(R.string.fcm_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(getString(R.string.fcm_token),recent_token); //stor token
        editor.commit();
    }
}
